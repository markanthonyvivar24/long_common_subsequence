"use strict";

function getLongestCommonSub(str1, str2) {
  let output = [];
  let subSeq1 = [];
  let subSeq2 = [];
  let maxLen = [];
  str1 = str1.toUpperCase();
  str2 = str2.toUpperCase();

  for (let i = 1; i < 2 ** str1.length; i++) {
    let strX = "";
    for (let j = 0; j < str1.length; j++) {
      if (i & (1 << j)) {
        strX += str1[j];
      }
    }
    subSeq1.push(strX);
  }
  // console.log(subSeq1)

  for (let i = 1; i < 2 ** str2.length; i++) {
    let strY = "";
    for (let j = 0; j < str2.length; j++) {
      if (i & (1 << j)) {
        strY += str2[j];
      }
    }
    subSeq2.push(strY);
  }
  // console.log(subSeq2)
  for (let i in subSeq1) {
    for (let j in subSeq2) {
      if (subSeq1[i] == subSeq2[j]) {
        output.push(subSeq1[i]);
      }
    }
  }
  // console.log(output)

  output.forEach((el) => {
    if (el.length === Math.max(...output.map((el) => el.length))) {
      maxLen.push(el);
    }
  });
  return [...new Set(maxLen)];
}

console.log(getLongestCommonSub("abcd", "abed"));
console.log(getLongestCommonSub("ajblqcpdz", "aefcnbtdi"));
